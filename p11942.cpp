#include <cstdio>
#include <iostream>
using namespace std;

int main()
{
    int n, arr[10], i, ck=0, ckk=0;

    scanf("%d", &n);
    printf("Lumberjacks:\n");

    for(int j=1; j<=n;++j){
        for(i=0; i<10;++i){
            scanf("%d", &arr[i]);
            if(i!=0){
                if(arr[i]>arr[i-1])
                    ck++;
                else if(arr[i]<arr[i-1])
                    ckk++;
            }
        }
        if(ck==9||ckk==9)
            printf("Ordered\n");
        else
            printf("Unordered\n");
        ck=0, ckk=0;
    }

    return 0;
}
